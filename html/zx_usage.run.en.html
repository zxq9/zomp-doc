<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="author" content="Craig Everett">
  <meta name="dcterms.rightsHolder" content="Craig Everett">
  <meta name="dcterms.rights" content="Copyright 2020 Craig Everett, all rights reserved">
  <meta name="dcterms.dateCopyrighted" content="2020">
  <link rel="stylesheet" type="text/css" href="zomp.css">
  <title>ZX Command: run</title>
</head>

<body>
<div id="body">
  <h1>ZX: run</h1>
  <p><code>zx run PackageID [Args]</code></p>
  <p>This is the most most common task of all: running a program.</p>
  <p>The "run" command accepts a single mandatory argument, a PackageID, and any number of following arguments meant to be passed to the application itself.</p>
  <p>In the context of this command the PackageID can be ambiguously specified. An omitted realm segment indicates the default open-source realm, "otpr". An omitted or partial version segment indicates the latest version available within the scope of whatever partial version is provided or the absolute latest if omitted entirely.</p>
  <p>Consider the following case:</p>
  <div class="source">
<pre>
ceverett@takoyaki:~$ zx list versions otpr-hello_world
0.1.0
0.1.1
1.0.0
1.0.1
1.1.0
1.1.1
ceverett@takoyaki:~$ </pre>
  </div>
  <p>To run the absolute latest version of the package "otpr-hello_world" the shortest command is:</p>
  <div class="source"><pre>zx run hello_world</pre></div>
  <p>The above command is exactly equivalent to:</p>
  <div class="source"><pre>zx run otpr-hello_world</pre></div>
  <p>The latest available version, v1.1.1, will be run.</p>
  <p>If the latest patch version of version 1.0 is desired, however, the command could be given with a partial version specifier:</p>
  <div class="source"><pre>zx run hello_world-1.0</pre></div>
  <p>Because the major and minor version segments are specified version selection is limited in scope to patch release versions of v1.0 of the program. In this case v1.0.1 will be automatically selected and run.</p>
  <p>If a program accepts command-line arguments then they can be added to the end of the command. The arguments are passed in as strings, split along whitespace, by the start procedure; arguments with spaces or escapes interpreted by the host shell environment must be quoted.</p>
  <div class="source">
<pre>ceverett@takoyaki:~$ zx run hello_world "Not Sure"
Hello, Not Sure! Hello, World!</pre>
  </div>

  <h2>The Procedure</h2>
  <p>The normal "run" procedure looks like this:</p>
  <ol>
    <li>ZX connects to its configured realms (if other instances are running it instead proxies through the "prime" instance).</li>
    <li>The latest matching target package version is queried if the PackageID does not have a complete version number.</li>
    <li>
      A check is made whether the (now fully qualified) target package is already installed on the system.
      <ul>
        <li>
          If not:
          <ol>
            <li>The source package (.zsp) fetched from Zomp.</li>
            <li>Its signature is verified.</li>
            <li>It is unpacked locally in the source cache.</li>
          </ol>
        </li>
      </ul>
    </li>
    <li>The target's dependency list is read from its zomp.meta file.</li>
    <li>The source cache is checked for each dependency; any not found are fetched as above.</li>
    <li>All unbuilt dependencies are built.</li>
    <li>If the target package is not already built, it is built now.</li>
    <li>The application is launched and its install directory is marked as the package home.</li>
    <li>Start args (if present) are stored by zx_daemon for retrieval.</li>
  </ol>
  <p>(See the <a href="zxl.en.html">zxl</a> page for how to interact with zx_daemon at runtime if you are a project developer.)</p>

  <h2>Including ZX Calls in Other Scripts</h2>
  <p>Any fully qualified version number will be respected, all the way down to the dependencies required by that specific version (there are no automatic dependency upgrades when running a program using Zomp/ZX). Because dependencies are statically defined by each package version and only missing packages and dependencies are queried and built when "run" is executed, a package and its dependencies are only subject to the query/fetch/build process the first time it is run. This means that one small (but in special cases significant) advantage of using a fully-qualified PackageID argument is that ZX does not even require network access to launch a program the second time it is called (or if it has been manually <a href="zx_usage.install.en.html">installed</a>):</p>
  <div class="source">
<pre>
ceverett@takoyaki:~$ time zx run otpr-hello_world-0.1.1
Hello, World!

real    0m1.863s
user    0m0.183s
sys     0m0.121s
ceverett@tahoyaki:~$ time zx run otpr-hello_world-0.1.1
Hello, World!

real    0m0.219s
user    0m0.134s
sys     0m0.036s
ceverett@takoyaki:~$ </pre>
  </div>
  <p>An almost 2 second difference doesn't really matter if you're starting a service or a GUI application you intend to run for more than a few seconds itself, but if you are including a ZX-launched program as part of a Bash script that may need to perform an action hundreds of times in a loop then the difference can be quite significant! Keep in mind that the time above includes the time to <em>build</em> and not just query the application (a second run using a partial version number would only do a query, as the latest version would already be built), but every time you start the first running instance of ZX on a system it is required to establish connections to all of its configured realms, and in the (rare) case that multiple redirects are required before it finds an available node for one or more realms the delay before finding a viable node could be significant.</p>
  <p>This leads to an important point about use of ZX launched programs in shell scripts: a preparatory query for the latest version is a good step prior to calling any ZX-launched program in a tight loop. In the below example we run a zx program iteratively in a Bash script, using ZX's "<a href="zx_usage.latest.en.html">latest</a>" query command to automatically generate a fully qualified PackageID:</p>
  <div class="source">
<pre>
#! /bin/bash
# Run the latest version of "Hello, World!" in a tight loop 10 times

package="otpr-hello_world"
version=$(zx latest "$package")
program="$package-$version"

for ((i=0;i&lt;10;i++))
do
    zx run "$program"
done
</pre>
  </div>
  <p>If there is a different hello_world package in a realm other than "otpr" then the realm name must <em>always</em> be specified:</p>
  <div class="source"><pre>zx run other_realm-hello_world</pre></div>
  <p>As before, the command above will run the latest version of the hello_world package available from the realm "other_realm", if such a package is available. Note, of course, that while the realm "otpr" is available by default on all ZX installations (unless explicitly <a href="zx_usage.drop_realm.en.html">removed</a>), any other realms will have to have been <a href="zx_usage.import_realm.en.html">added</a> to the Zomp/ZX config by a user or site administrator to make that realm "visible" to ZX.</p>
</div>
<footer>
  <nav><a href="index.html">Top</a> | <a href="zx_usage.en.html">ZX Usage</a></nav>
  <p><small>&copy; 2020 Craig Everett</small></p>
  <p><small><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"><img alt="Creative Commons License" style="border-width:0" src="./by-nc-nd.png" /></a><br>
  This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.</small></p>
</footer>
</body>
</html>