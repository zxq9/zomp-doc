<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="author" content="Craig Everett">
  <meta name="dcterms.rightsHolder" content="Craig Everett">
  <meta name="dcterms.rights" content="Copyright 2020 Craig Everett, all rights reserved">
  <meta name="dcterms.dateCopyrighted" content="2020">
  <link rel="stylesheet" type="text/css" href="zomp.css">
  <title>Zomp/ZX Files</title>
</head>

<body>
<div id="body">
  <h1>Zomp/ZX File Definitions</h1>
  <p>Zomp and ZX make use of the same directory locations, configuration files, caches, bundles and locks. While it should not be necessary for users to go digging through any of these as they all have programmatic interfaces exposed via ZX, having clear file definitions and explanations can assist others in writing additional tools that augment Zomp and ZX. (It is also a handy reference for Zomp and ZX devs...)</p>

  <h2>Directory Layout</h2>
  <p>ZX installs itself by default to a location equivalent to <code>$HOME/zomp</code> (or <code>%LOCALAPPDATA%\zomp</code> in crazyland). An environment variable <code>$ZOMP_DIR</code> is defined by the ZX start script to refer to this location and can be set prior to calling the zx start script to indicate a custom install location.</p>
  <p>The layout of the <code>$ZOMP_DIR</code> is as follows:</p>
  <div class="source">
<pre>
<a href="#zx">zx</a>
<a href="#zxh">zxh</a>
<a href="#john_locke">john.locke</a>  (if running)
etc/
    <a href="#sys_conf">sys.conf</a>
    <a href="#version_txt">version.txt</a>
    [Realm Name]/
        <a href="#realm_conf">realm.conf</a>
        <a href="#user_conf">[username].user</a>
        [Package Name]/
            [Version]/
var/
    [Realm Name]/
        <a href="#history_log">history.log</a>
        <a href="#realm_cache">realm.cache</a>
        <a href="#realm_stash">realm.stash</a>
        [Package Name]/
            [Version]/
tmp/
    [Realm Name]/
        [Package Name]/
            [Version]/
log/
    [Realm Name]/
        [Package Name]/
            [Version]/
key/
    [Realm Name]/
zsp/
    [Realm Name]/
lib/
    [Realm Name]/
        [Package Name]/
            [Version]/
</pre>
  </div>
<!-- zx.ico -->
  <p>The <code>zx</code> and <code>zxh</code> files are bash scripts that prepares the local environment for ZX to launch on a *nix type system. (A satirical equivalent called <code>zx.cmd</code> once existed, but the Windows launch strategy is changing considerably due to the weirdness inherent in that world.) The file <a href="#john_locke"><code>john.locke</code></a> is a lockfile used by ZX to determine whether another instance is running and how to contact it; ZX proxies file and network operations through the longest running instance to prevent conflicts.</p>
  <p>The <code>etc/</code>, <code>var/</code>, <code>tmp/</code> and <code>log/</code> directories are provided as known locations for the use of ZX launched programs and take on their usual role within a system (they are free to ignore these, though). Inside of each a directory is created per realm, within which realm-level configuration and data is stored as well as a directory created per application/version that requires persistent configuration and data locations when run. An application can query zx for the absolute path to its config and data directories. The main setting file, <code>sys.conf</code> and the version indicator are the only files in the top-level of <code>etc/</code>.</p>
  <p>The <code>key/</code> directory contains a subdirectory per realm, and inside stores public and private keys in DER format as <code>[Key ID].key.der</code> and <code>[Key ID].pub.der</code> respectively. A key's global ID is the SHA512 hash of the public half of the key.</p>
  <p>The <code>zsp/</code> directory contains a subdirectory per realm and inside caches Zomp source packages as <code>[Realm Name]-[Package Name]-[Version].zsp</code>.</p>
  <p>The <code>lib/</code> directory contains a subdirectory per realm, inside which a directory per package is created, inside which a directory per version is created. Source packages are unpacked to their version directory first and then built and installed in place. Beam code for launched applications and specified dependencies is loaded directly by ZX from the installed locations in <code>lib/</code>.</p>

  <h2>Files</h2>
  <p>There are seven basic categories of files used by the system:</p>
  <ol>
    <li>Start Scripts
      <ul>
        <li><a href="#zx">zx</a></li>
        <li><a href="#zxh">zxh</a></li>
      </ul>
    </li>
    <li>The Lock File
      <ul>
        <li><a href="#john_locke">john.locke</a></li>
      </ul>
    </li>
    <li>Configuration Files
      <ul>
        <li><a href="#sys_conf">etc/sys.conf</a></li>
        <li><a href="#version_txt">etc/version.txt</a></li>
        <li><a href="#realm_conf">etc/[Realm Name]/realm.conf</a></li>
        <li><a href="#user_conf">etc/[Realm Name]/[User Name].user</a></li>
      </ul>
    </li>
    <li>Bundles
      <ul>
        <li><a href="#realm_files">Realm Files</a> (.zrf files)</li>
        <li><a href="#public_user_files">Public User Files</a> (.zpuf files)</li>
        <li><a href="#dangerous_user_files">Dangerous User Files</a> (.zduf files)</li>
        <li><a href="#zsp">Source Packages</a> (.zsp files)</a></li>
      </ul>
    </li>
    <li>Caches
      <ul>
        <li><a href="#history_log">var/[Realm Name]/history.log</a></li>
        <li><a href="#realm_cache">var/[Realm Name]/realm.cache</a></li>
        <li><a href="#realm_stash">var/[Realm Name]/realm.stash</a></li>
      </ul>
    </li>
    <li>Key Files
      <ul>
        <li><a href="#pub">key/[Realm Name]/[Key Name].pub.der</a></li>
        <li><a href="#key">key/[Realm Name]/[Key Name].key.der</a></li>
      </ul>
    </li>
    <li>Project Meta and Utilities
      <ul>
        <li><a href="#zomp_meta">[Project Dev Dir]/zomp.meta</a></li>
        <li><a href="#zmake">[Project Dev Dir]/zmake</a></li>
        <li><a href="#app_files">[Project Dev Dir]/ebin/[App Name].app</a></li>
      </ul>
    </li>
  </ol>

  <h3>Start Scripts</h3>
  <p>The reason for having two different start scripts is to provide users (quite often developers) the option of running an application in "headless" (no erl shell attached) or "shell" mode.</p>

  <h4 id="zx">The zx Start Script</h4>
  <p>Starts in headless mode. The common way for non-developers to run programs.</p>

  <h4 id="zxh">The zxh Start Script</h4>
  <p>Starts a program with an erl shell attached (most useful for developers).</p>

  <h3 id="john_locke">john.locke</h3>
  <p>This is the environment-wide lock file for ZX instances. This file contains a port number listening on localhost that any new zx runtime can connect to and use as a proxy for operations that involve external resources:</p>
  <p>This is how multiple ZX instances running concurrently from the same $ZOMP_DIR <a href="protocol_1.en.html#multiple_instances">avoid clobbering each other</a>.</p>

  <h3>Configuration Files</h3>
  <p>Zomp has a handful of configuration files it uses to keep track of the state of the system. Users should not have to manipulate them by hand (all have a command-line interface via ZX), though most are intended to be read with <a href="http://erlang.org/doc/man/file.html#consult-1"><code>file:consult/1</code></a> and incidentally text-editor friendly. Compared to most systems Zomp/ZX files are simple to the point of triviality, and defaults can be restored to sanity with a simple command.</p>

  <h4 id="sys_conf">etc/sys.conf</h4>
  <p>The system-wide configuration file. This file is actually not necessary, and if it or attributes within it are missing the file will be fixed by ZX at start time and populated with defaults.</p>
  <p>The default version of the file looks like this:</p>
  <div class="source">
<pre>
{timeout,     5}.
{retry,       3}.
{maxconn,     5}.
{managed,     []}.
{mirrors,     []}.
{status,      listen}.
{node_max,    16}.
{vamp_max,    16}.
{leaf_max,    256}.
{listen_port, 11311}.
{public_port, 11311}.
</pre>
  </div>
  <p>The <code>timeout</code> attribute is the base used to determine timeouts throughout the system. Most operations within the system have a timeout factor of 1000 milliseconds, meaning that most timeouts occur within 5 seconds as <code>5 * 1000ms = 5s</code>.
  <p>The <code>retry</code> attribute tells the local node (whether ZX or Zomp) how many times to retry a remote action before giving up. This means that a ZX node will perform a connection attempt cycle to a needed realm this many times before giving up on a realm and reporting failure to the user (meaning, exhausting private mirrors, the prime node, and cached hosts X times). It also means that ZX or Zomp will attempt a fetch this many times before giving up, and because a fetch failure always results in a disconnect this means the fetch will be tried on this many different hosts before giving up. A high value has a higher chance of success overall, but also increases the wait time a user will experience if there are major network issues upstream.</p>
  <p>The <code>maxconn</code> attribute controls how many parallel connection attempts can be made per configured realm. The default value is almost never reached in practice except on huge realms with scores of known cached hosts.</p>
  <p>The <code>managed</code> attribute is a list of what realms this node serves as the prime node: its "managed" realms. As most nodes are not primes this always defaults to an empty list. The managed realms list can be updated with the <a href="zx_usage.zx_takeover.en.html"><code>zx takeover RealmName</code></a> and <a href="zx_usage.zx_abdicate.en.html"><code>zx abdicate RealmName</code></a> commands.</p>
  <p>The <code>mirrors</code> attribute is a list of Zomp mirrors that should be tried first. This attribute is usually set by network administrators in organizations to declare internal mirrors as high-speed options. The mirrors list can be viewed and edited with the <a href="zx_usage.add_mirror.en.html"><code>zx add mirror [address [port]]</code></a> command.</p>
  <p>The <code>node_max</code> threshold makes it so that if new publicly accessible Zomp nodes are connecting and the threshold has not yet been reached, then the new connections are accepted regardless what realms they are requesting. If the threshold has been reached but some downstream nodes are not enrolled in realms for which this node is acting as prime and the connecting node lists the prime realm as an enrollment target then a downstream node that is not enrolled in a prime realm will be redirected to open a slot for the new connection. If all slots are full and the previous condition is not met then the new connection is immediately redirected.</p>
  <p>The <code>vamp_max</code> threshold tells the node how many parasitic connections to accept. Most nodes are publicly visible and help spread the load out across the network which helps with redundancy and availability. A vampire connection is one that isn't publicly accessible for whatever reason, whether its public port is set to 0 (which indicates to upstream nodes that it is not reachable) or it simply can't be reached when the upstream node does its visibility check when the NODE session is established. The good news about data vampires is that they tend to be <em>very</em> low traffic clients which actually reduces external load across the entire public network.</p>
  <p>The <code>leaf_max</code> threshold makes it so that new connections are accepted up to the threshold limit, and then redirected if downstream nodes exist for requested realms. If no downstream nodes exist for realms this node acts as prime, then the connection is accepted anyway.</p>
  <p>The port settings are important to understand. Zomp listens on a port locally, but for that port to be visible to the outside world you almost always have to configure your network to forward traffic from a port on your public address to the port on the machine inside your network that is running Zomp. These might not be the same port numbers, so Zomp lets you designate two: the publicly visible one (that it will announced to other nodes when connections are made) and the local one actually being listened on.</p>

  <h4 id="version_txt">etc/version.txt</h4>
  <p>This file contains a string representation of the current version of ZX on the system. This indicates to the ZX start scripts which version of ZX should be run and therefore how the path to it should be formed.</p>

  <h4 id="realm_conf">etc/[Realm Name]/realm.conf</h4>
  <p>Each realm has a configuration file that contains enough information to get a realm started from zero. This file defines the realm name, the prime host, the realm's root key (the first one the sysop created), and the user ID of the sysop who created the realm.</p>
  <div class="source">
<pre>
{realm,     <a href="types.en.html#zx_realm">zx:realm()</a>}.
{prime,     <a href="types.en.html#zx_host">zx:host()</a>}.
{sysop,     <a href="types.en.html#zx_user_name">zx:user_name()</a>}.
{key,       <a href="types.en.html#zx_key_name">zx:key_name()</a>}.
{timestamp, <a href="http://erlang.org/doc/man/calendar.html#type-datetime">calendar:datetime()</a>}.
</pre>
  </div>
  <p>The realm.conf file is extracted and installed from a <a href="#realm_files">realm file</a> when the command <a href="zx_usage.import_realm.en.html"><code>zx import realm RealmFile</code></a> is run.</p>

  <h4 id="user_conf">etc/[Realm Name]/[User Name].user</h4>
  <p>User data files represent the local user(s) information relative to the realm. Only users with a packager, manager, or sysop role in a realm require a user account (most of what ZX clients and all of what Zomp distribution nodes do is anonymous in the open source version). Creating multiple users within a single realm in a single ZX installation is possible, but unusual. In the case multiple users exist ZX will prompt the user to pick one before executing any commands that require authentication.</p>
  <div class="source">
<pre>
{realm,        <a href="types.en.html#zx_realm">zx:realm()</a>}.
{username,     <a href="types.en.html#zx_user_name">zx:user_name()</a>}.
{realname,     <a href="http://erlang.org/doc/reference_manual/data_types.html#id77669">string()</a>}.
{contact_info, [<a href="types.en.html#zx_contact_info">zx:contact_info()</a>]}.
{keys,         [<a href="types.en.html#zx_key_name">zx:key_name()</a>]}.
</pre>
  </div>
  <p>The data contained in a user file is basic, but combined with the realm name provides enough information to uniquely identify and therefore locate everything necessary to interact with a prime node on behalf of the user.</p>
  <p>User files are created when the <a href="zx_usage.create_user.en.html"><code>zx create user Realm</code></a> command is run.</p>
  <p>This file is rolled into an <a href="#user_files">archive</a> with relevant user keys when the <a href="zx_usage.export_user.en.html"><code>zx export user</code></a> command is run.</p>
  <p>This file is added to a local installation with the <a href="zx_usage.import_user.en.html"><code>zx import user UserFile</code></a> command.</p>

  <h3>Bundles</h3>
  <p>Several files used by the system are archives. These bundles simplify tasks like transferring source files as signed packages and shipping around configuration data in a way that is relatively simple for users to deal with. All of these bundle files are the target of certain ZX commands.</p>

  <h4 id="realm_files">Realm Files (.zrf files)</h4>
  <p>A "realm file" is a bundle that contains the <a href="#realm_conf"><code>etc/[Realm Name]/realm.conf</code></a> file necessary to configure the local system  and the realm's root key. The commands <a href="zx_usage.create_realm.en.html"><code>zx create realm</code></a> and <a href="zx_usage.export_realm.en.html"><code>zx export realm RealmName</code></a> both produce a realm file named "[RealmName].zrf" in the current working directory, and the command <a href="zx_usage.import_realm.en.html"><code>zx import realm RealmFile</code></a> installs a realm from the realm file at the path provided to the command.</p>
  <p>A realm file is simply an Erlang external binary containing:</p>
  <ul>
    <li><a href="#realm_conf">etc/[Realm Name]/realm.conf</a></li>
    <li><a href="#pub">key/[Realm Name]/[KeyID].pub.der</a></li>
  </ul>
  <p>The procedure to create the .zrf binary is:</p>
  <div class="source">
<pre>
{ok, RealmConf} = zx_lib:load_realm_conf(Realm)
{ok, KeyDer} = file:read_file(KeyPath)
Blob = term_to_binary({RealmConf, KeyDER})
ok = file:write_file(ZRF, Blob)
</pre>
  </div>
  <p>When a new realm is created the person who created it is the new sysop. The sysop either makes the .zrf file available publicly on a website or forum, or gets it into the hands of users some other way (email, configuration daemon, embedded in a VM or Docker image, etc.).</p>

  <h4 id="public_user_files">Public User Files (.zpuf files)</h4>
  <p>A public user file contains a binary representation of a tuple where <a href="#user_conf">user's data file</a> is the first element and the user's and public key data (key names and the actual DER data) are the second. A .zpuf contains only public keys and is therefore the safe way to send a user profile to a sysop for addition to a realm.</p>
  <p>This file can be created by using the command <a href="zx_usage.export_user.en.html"><code>zx export user</code></a>.</p>
  <p>When someone wants to get a user account on a realm they do:
  <ol>
    <li>Run <a href="zx_usage.create_user.en.html"><code>zx create user</code></a> <em>on their own computer</em>, making sure to select the realm to which they want to be added.</li>
    <li>Run <code>zx export user</code>, selecting the realm/user which was just created (if more than one exists).</li>
    <li>Send the resulting <code>[realm]-[user].zpuf</code> file to the sysop of the realm.</li>
  </ol>
  <p>The sysop can then install it on the realm (using <a href="zx_usage.add_user.en.html"><code>zx import user</code></a>) without coming into contact with the user's private keys.</p>
  <p>The internal structure of the file adheres to:</p>
  <div class="source">
<pre>
{ok, Bin} = file:read_file(Zpuf)
{UserData, PubKeyData} = binary_to_term(Bin)
UserData   :: [{Attribute :: atom(),  Value :: term()}]
PubKeyData :: [{KeyName :: zx:key_name(), DER :: binary()}]
</pre>
  </div>
  <p>The UserData element above is the result of <code>file:consult/1</code> on the user's <code>etc/[User Name].user</code> file. The contained data gets written to the correct locations as part of the import/add procedure.</p>

  <h4 id="dangerous_user_files">Dangerous User Files (.zduf files)</h4>
  <p>While the .zpuf files contain only public data, the .zduf files contain <em>dangerous</em> data. The difference is whether the user's private keys are present in the bundle.</p>
  <p>These files can be created with the <a href="zx_usage.export_user.en.html"><code>zx export user dangerous</code></a> command and should only ever be handled by the user who owns the data. Never send it to sysop or store it on an unsecure device.</p>
  <p>A user can use this file with <a href="zx_usage.import_user.en.html"><code>zx import user</code></a> to add his account (including private credentials) to another computer or ZX install location. This is very similar to moving SSH keys and GitLab credentials and settings around, but in this case the metadata moves with you and there is a little bit less to mess with than.</p>
  <p>The internal structure of the file fits:</p>
  <div class="source">
<pre>
{ok, Bin} = file:read_file(Zduf)
{UserData, PubKeyData} = binary_to_term(Bin)
UserData :: [{Attribute :: atom(), Value :: term()}]
KeyData  :: [{KeyName :: zx:key_name(), KeyDER :: binary(), PubDER :: binary()}]
</pre>
  </div>

  <h4 id="zsp">Source Packages (.zsp files)</h4>
  <p>The whole point of this circus.</p>
  <p>These are binary strings with three major segments. It is easier to show than try to explain:</p>
  <div class="source">
<pre>
&lt;&lt;SigSize:24, Sig:SigSize/binary, Signed/binary&gt;&gt; = ZRP
&lt;&lt;MetaSize:16, MetaBin:MetaSize/binary, TarGz/binary&gt;&gt; = Signed
{PackageID, KeyName, Modules} = binary_to_term(MetaBin, [safe])
</pre>
  </div>
  <p>The Modules element in the tuple above is a manifest of all module names (as strings, not atoms) that are defined by the package.</p>
  <p>The project itself is contained in the TarGz archive in the binary.</p>
  <p>Package files are named according to their realm, package name and version number, then have the .zsp extension tacked on. So the package for the project "jumptext" from the repository "otpr" as of version 0.1.0 is: <code>otpr-jumptext-0.1.0.zsp</code>. Each realm has a package cache at <code>zsp/[Realm Name]/</code> where source packages are cached once fetched and verified by signature.</p>
  <p>Source packages submitted for review are the same as source packages for program installation from a realm, with the exception that submitted packages are signed by packagers, not sysops, and only available with the <a href="zx_usage.review.en.html"><code>zx review PackageID</code></a> command. That command extracts the package <em>in place</em> for inspection and use with the <a href="zx_usage.rundir.en.html"><code>zx rundir</code></a> command, not to the local system's <code>lib/</code>.</p>

  <h3>Caches</h3>
  <p>ZX and Zomp have to keep track of the current state of their configured realms to work efficiently. To find distribution nodes quickly ZX uses a caching strategy that keeps track of known redirect targets to which it attempts connections in parallel with attempts made to the prime node of each realm it is supposed to contact -- whichever succeeds first. Zomp must keep a complete record of the realm history for every realm it is configured so that it can provide missing bits of the realm's history to downstream requestors. Building the current index state out of a very long history is a bit cumbersome, so instead Zomp stashes a current snapshot of each realm's index data in the filesystem whenever it shuts down or has been idle for a minute and has a stale state stash.</p>
  <p>All of these files can be replaced if corrupted or missing, the cost is some processing effort.</p>

  <h4 id="history_log">var/[Realm Name]/history.log</h4>
  <p>A realm's history log is a binary term version of the hash map that is maintained internally by the Zomp realm registry processes. The storage procedure is to write the output of <a href="http://erlang.org/doc/man/erlang.html#term_to_binary-2">term_to_binary/2</a> to this location, and reading the file is to read the contents of the file and pass it through <a href="http://erlang.org/doc/man/erlang.html#binary_to_term-2">binary_to_term/2</a>. If anything about storing, loading, reading, etc. this file or the data contained inside is corrupted or unworkable the node will reset its serial to 0 and request a full sync of the realm's current history. The advantage of this approach is that the upstream node can simply <a href="protocol_1.en.html#node_refresh_history">send a copy</a> of its current history.log instead of sending updates <a href="protocol_1.en.html#node_pull_event">piece by piece</a>.</p>

  <h4 id="realm_cache">var/[Realm Name]/realm.cache</h4>
  <p>Realm cache files are where ZX keeps track of a realm's current serial and known mirrors. If the realm cache is corrupted or lost it is discarded and another one will be generated in its place. If mirror connection attempts don't work out they are purged. If a prime node is connected and its serial is <em>behind</em> the currently tracked one the node assumes itself to be in error and resets the serial to whatever the prime claims it is.</p>
  <p>The inside of a cache file contains the following:</p>
  <div class="source">
<pre>
{serial,  non_neg_integer()}.
{mirrors, [<a href="#zx_host">zx:host()</a>]}.
</pre>
  </div>
  <p>Zomp realms are replicated in a degenerate, trickle-down sort of way. Every realm has a prime node (its prime server), any node may be prime for any number of realms, and any node may additionally mirror any number of other realms. Zomp nodes can also be used as organizational-internal private mirrors to speed things up in the case that a large number of users resides behind a single NAT interface or when a realm is entirely private to an organization. Because Zomp nodes come on and off the network at random, only the prime node and private mirrors are expected to be (somewhat) reliably available.</p>

  <h4 id="realm_stash">var/[Realm Name]/realm.stash</h4>
  <p>Zomp maintains the functional version of realm state as a set of interrelated index structures. Each realm has its own process that maintains this state. Building the state is a bit cumbersome on really large histories, so instead of rebuilding it every time something changes whenever the history.log is updated the indices are serialized and written to the realm's stash file. This speeds up start time considerably on large realms over rebuilding the indices from scratch using the history.log every time a node is started. If the realm stash is corrupted it is discarded and a new one is generated from the history.log.</p>
  <p>The details of exactly how this works is subject to change, so for details on the current state of madness please refer to the zomp_realm.erl sources.</p>

  <h3>Key Files</h3>
  <p>Key files are <a href="https://tls.mbed.org/kb/cryptography/asn1-key-structures-in-der-and-pem">DER-format</a> files located in a path equivalent to <code>key/[Realm Name]/[Key Name].[Type].der</code> where the "Key Name" is usually "[User]-[Label]" and "Type" is "pub" for public keys and "key" for private keys. For example, the root public key for OTPR is <code>key/otpr/zxq9-root.pub.key.der</code>.<p>
  <p>In the normal case a software user will only need the realm and package keys provided with the initial realm configuration bundle.</p>

  <h4 id="pub">key/[Realm Name]/[Key Name].pub.der</h4>
  <p>The public half of an RSA keypair as used by the system. The only thing of special note is how the name is derived. A <a href="types.en.html#zx_key_id">key ID</a> looks like this: <code>{Realm, KeyHash}</code> where Realm is an ASCII string and the KeyHash is the binary SHA512 hash of the public half of the key (which can always be derived from either half). The path to the key can therefore be derived from its ID (the hash converted to base 36) with the extension ".pub.der" or ".key.der" added.</p>

  <h4 id="key">key/[Realm Name]/[Key Name].key.der</h4>
  <p>The private half of an RSA keypair as used by the system. The naming convention is the same as the public half, but the extension is ".key.der" in this case. These should <em>never</em> be exposed off of the user's system to whom they belong. Nothing in Zomp or ZX <em>ever</em> requires private keys to travel from a user's system -- even a realm's prime node is completely void of private keys unless it happens to be the personal computer of the sysop and he is hosting it directly (which isn't unheard of, but is a bit unusual outside of an organizational or short-term community setting like hosting a realm inside a small business or at a conference or hackathon).</p>

  <h3>Project Meta and Utilities</h3>
  <p>ZX provides a few utilities and conveniences for developers to manage projects just a little bit easier (or that's the intent, anyway). To make it easy for ZX to do its job without too much guessing a bit of meta about a project is kept within the project's root directory, the Erlang .app file is maintained in the course of this, and an optional utility script can be put in place by a developer to make things happen like initiating native code builds (like <a href="http://erlang.org/doc/tutorial/nif.html">NIF</a>s in <a href="https://www.rust-lang.org/">Rust</a> or C) or whatever else might need to happen at build time.</p>
  <p>Not listed here but necessary is an <a href="http://erlang.org/doc/man/make.html#id89565">Emakefile</a> (a very simple version of which will be created if it doesn't already exist).</p>

  <h4 id="zomp_meta">[Project Dev Dir]/zomp.meta</h4>
  <p>A <code>zomp.meta</code> file must exist in the root directory of any Zomp-compatible project. A zomp.meta file lets Zomp know how to handle the project, how to build it, where to put it, what version it is, and other information required to describe the project. A zomp.meta file is a list of Erlang terms readable by file:consult/1 (it is a properly list), though internally it is converted to an Erlang map.</p>

  <h4 id="zmake">[Project Dev Dir]/zmake</h4>
  <p>This file is optional and will be called via <a href="http://erlang.org/doc/man/os.html#cmd-1">os:cmd/1</a> if present. This must be placed in the root directory of the project or it will not be found. Typically this is a Bash script, and the most common reason for it to exist is to initiate the building of NIF or <a href="https://mercurylang.org/">Mercury</a> code (a task ZX is not yet ready to integrate on its own just yet).</p>
  <p>Code built using zmake should go in the <a href="http://erlang.org/doc/man/code.html#priv_dir-1">usual location</a>, but there are no contraints on what zmake might do. As a packager or developer this is your omni-hook and gives you complete freedom to make magical things work with ZX (or totally blow up the user's system -- so be careful about your user assumptions!).</p>

  <h4 id="app_files">[Project Dev Dir]/ebin/[App Name].app</h4>
  <p><a href="erlang.org/doc/man/app.html">Erlang .app files</a> are part of how OTP makes sure its application management procedures are doing what they are supposed to do. ZX is compliant with OTP, though it doesn't deploy code as <a href="erlang.org/doc/design_principles/release_structure.html">pre-built releases</a> the way Erlang was traditionally used. Everything about Erlang releases is perfect for deploying code to phone switches or other embedded devices that require highly robust behavior, but it isn't quite as good a fit for programmers deploying web services, game servers, messaging infrastructure, and client-side utility and GUI code. Everything about Erlang/OTP is <em>awesome</em> for taking full advantage of modern ridiculously-multi-core hardware and inherently networked systems, though -- so Zomp/ZX makes an attempt to strike a balance between the two worlds. That means it must comply with OTP expectations, and that means keeping the .app file up to date.</p>
  <p>The most important thing ZX does with an .app file is keeping the current version number up to date with the version in zomp.meta, and (probably more important) keeping the "modules" list accurate. ZX will not, however, modify anything else in the file aside from when it is initially written if the project was created with ZX.</p>
</div>
<footer>
  <nav><a href="index.html">Top</a> | <a href="contents.en.html">Contents</a></nav>
  <p><small>&copy; 2020 Craig Everett</small></p>
  <p><small><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"><img alt="Creative Commons License" style="border-width:0" src="./by-nc-nd.png" /></a><br>
  This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.</small></p>
</footer>
</body>
</html>
