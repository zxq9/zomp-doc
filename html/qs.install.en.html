<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="author" content="Craig Everett">
  <meta name="dcterms.rightsHolder" content="Craig Everett">
  <meta name="dcterms.rights" content="Copyright 2020 Craig Everett, all rights reserved">
  <meta name="dcterms.dateCopyrighted" content="2020">
  <link rel="stylesheet" type="text/css" href="zomp.css">
  <title>ZX Quickstart: Desktop Shortcuts</title>
</head>

<body>
<div id="body">
  <h1>ZX: Installation</h1>
  <p>Before you can use ZX you need two things:</p>
  <ol>
    <li><a href="https://www.erlang.org/downloads/">The Erlang runtime</a> (version R21 or above)</li>
    <li><a href="download.en.html">ZX</a></li>
  </ol>

  <h3>Getting Erlang</h3>
  <h4>For *nix users (Linux, Unix, BSD, OSX, etc.)</h4>
  <p>You have a few options:</p>
  <ul>
    <li>Use <a href="https://github.com/kerl/kerl">Kerl</a> to build the the latest version(s) tailored to your system/arch.<br>
        (Most common option among developers. Example: <a href="/archives/1797">Building Erlang R23.0 on Debian/Ubuntu</a>)</li>
    <li>Install a pre-built binary from <a href="https://www.erlang-solutions.com/resources/download.html">Erlang Solutions</a>. (More info at the <a href="https://www.erlang.org/downloads">Erlang downloads page</a>.)</li>
    <li>Install a pre-built binary from your distro repository. (Note: These may be a year or two out of date.)</li>
    <li>Build and install from source on your own. (If Kerl is unsupported on your platform.)</li>
  </ul>
  <h4>For Windows users</h4>
  <p>Go to the <a href="https://www.erlang.org/downloads">Erlang downloads page</a> and get the Windows installer that suits your system.<br>
  (Protip: most modern systems are 64-bit.)</p>

  <h3>Getting ZX</h3>
  
  <h4>The Sith Method</h4>
  <p>If you are on a unix type system and already have Erlang installed, the easiest way to install ZX is by running the following command and you'll be all set!<p>
  <div class="source"><pre>wget -q https://zxq9.com/projects/zomp/get_zx &amp;&amp; bash get_zx</pre></div>
  <p>(The command above simply automates the steps shown below.)</p>

  <h4>The Jedi Path</h4>
  <p>Go to the <a href="download.en.html">ZX download page</a> and get the installation bundle. Follow the instructions for *nix or Windows below.</p>

  <h5>Linux/BSD/OSX/Unix/*nix</h5>
  <p>Steps:<p>
  <ol>
    <li><code>wget https://zxq9.com/projects/zomp/zx-0.11.6.tar.xz</code></li>
    <li><code>tar -Jxf zx-0.11.6.tar.xz</code></li>
    <li><code>cd zx-0.11.6</code></li>
    <li><code>chmod +x install</code></li>
    <li><code>./install</code></li>
  </ol>
  <p>Note: If you do not have XZ on your system you'll need the gzipped bundle.</p>
  <p>Here is a log of what you <em>should</em> experience should everything go well (<code>-q</code> is used to silence wget's output -- it does not change anything):</p>
  <div class="source">
<pre>
everett@cake:~$ wget -q https://zxq9.com/projects/zomp/zx-0.11.6.tar.xz
everett@cake:~$ tar -Jxf zx-0.11.6.tar.xz 
everett@cake:~$ cd zx-0.11.6/
everett@cake:~/zx-0.11.6$ ./install 
Erlang found at /home/everett/.erts/23.0/bin/erl
/home/everett/bin was found in $PATH. Good to go.
Path adjustment found in /home/everett/.profile.
Path adjustment found in /home/everett/.bashrc.
No /home/everett/.bash_profile is present. Skipping.
zx found at /home/everett/bin/zx. Checking for upgrade.
Running `zx upgrade`...
Recompile: src/zx_zsp
Recompile: src/zx_userconf
Recompile: src/zx_tty
Recompile: src/zx_sup
Recompile: src/zx_proxy
Recompile: src/zx_peers
Recompile: src/zx_peer_sup
Recompile: src/zx_peer_man
Recompile: src/zx_peer
Recompile: src/zx_net
Recompile: src/zx_local
Recompile: src/zx_lib
Recompile: src/zx_key
Recompile: src/zx_daemon
Recompile: src/zx_conn_sup
Recompile: src/zx_conn
Recompile: src/zx_auth
Recompile: src/zx
Current version: otpr-zx-0.11.6
Running latest version.
everett@cake:~/zx-0.11.6$ cd ..
everett@cake:~$ rm -rf zx-0.11.6*</pre>
  </div>
  <p>From this point you should be able to use ZX from the command line!</p>
  <p>YAY! You're ready to <a href="quickstart.en.html">do stuff!</a></p>
  <p>It is a good idea to run <code>zx upgrade</code> periodically to make sure you have the latest version.</p>

  <h5>Windows</h5>
  <p>Windows interfaces with ZX and Zomp via a graphical front-end called <a href="https://gitlab.com/zxq9/vapor">Vapor</a>. The Windows installer for ZX is called "<a href="InstallVapor.exe">InstallVapor.exe</a>" for this reason, and installs both ZX and Vapor, adding links from the desktop to Vapor.</p>
  <p>Launching programs through Vapor is perfectly comfortable, but many developer functions are difficult to make work properly with the Windows cmd.exe terminal (though Bash implementations such as the one that comes with "Git for Windows" make things <em>much</em> easier). Until Vapor's support extends to the developer features, it is best to consider Windows as an "end-user" platform and not suitable for developers using ZX just yet.</p>
  <p> ZX commands can be used from the command line in Windows, but currently require being invoked from the user's <code>%USERPROFILE%</code> (usually <code>C:\Users\[username]\</code> -- wherever cmd.exe starts by default) as <code>zx.lnk [command]</code> or <code>zxh.lnk [command]</code> instead of the cleaner <code>zx [command]</code>. The reason for this is that the Vapor installer does not update your Windows <code>%PATH%</code> variable. (You may update your path to include <code>%LOCALAPPDATA%\zomp\</code>, however.)</p>
  <p>Vapor on Windows is still itself in beta, so you will encounter a few rough edges, such as the console window being opened when you click "Vapor", and a slowing expanding feature set. The good news is that Vapor will automatically update itself every time it is run.</p>

  <h2>Uninstallation</h2>
  <h3>Linux/BSD/OSX/Unix/*nix</h3>
  <p>Run <code>$HOME/zomp/uninstall</code>. It will remove the symlinks in <code>$HOME/bin</code> and the entire <code>$HOME/zomp</code> directory.</p>

  <h3>Windows</h3>
  <p>The installer registers Vapor with the Windows registry the normal way and provides an uninstaller. Select "uninstall" from the normal "Applications" configuration menu within Windows, or find <code>C:\Users\[username]\AppData\Local\zomp\uninstall.exe</code> and run it.</p>

  <h2>Unix Notes for Developers and Sysops</h2>
  <p>ZX installs to <code>$HOME/zomp/</code> and creates symlinks to zx (headless) and zxh (with erl shell) at <code>$HOME/bin/</code> by default. This behavior can be changed, of course, and those wishing to run system services built and launched by ZX will usually want to move their installations to a custom location (creating a service user account and installing ZX as that user with <code>$HOME</code> set to a custom location simplifies this).</p>
  <p>The start script for ZX sets an environment variable <code>$ZOMP_DIR</code> to <code>$HOME/zomp/</code> if it is not already set. If you want to install to a custom location that is <em>not</em> the <code>$HOME</code> of the user account that will be executing it (for system services, for example) then your start procedure will need to set the environment variable before calling the zx launch script.</p>
  <p>The main thing to remember is that ZX doesn't care <em>where</em> it is installed or <em>who</em> it is run by because it should <em>never</em> ask for system privileges and only requires that the execution scripts can locate an Erlang runtime.</p>
  
  <h2>Doing Stuff</h2>
  <p>Once you've got an Erlang runtime and ZX in your <code>$PATH</code>, you can do stuff (like <a href="qs.run.en.html">running programs</a>!).</p>
  <p>A list of common tasks and workflows is back on the <a href="quickstart.en.html">quickstart page</a>.</p>
  <p>The comprehensive <a href="zx_usage.en.html">ZX user manual page</a> has more technically exhaustive explanations and covers every command, but not everyone really needs to read all of it.</p>

  <h2>Feedback</h2>
  <p>To provide feedback directly to the author look for me (Craig Everett, aka "zxq9") at:</p>
  <ul>
    <li>#erlang or #zomp on IRC at <a href="https://www.oftc.net/">OFTC.net</a></li>
    <li>#erlang or #erlounge on IRC at <a href="https://freenode.net/">Freenode</a></li>
    <li><a href="https://chat.stackoverflow.com/rooms/75358/erlang-otp">The Erlang/OTP room</a> on StackOverflow chat</li>
    <li>The "Erlangers" team on <a href="https://keybase.io/">Keybase</a></li>
    <li>The <a href="https://erlanger.slack.com">Erlanger Slack</a> (new invitations seem to only work through <a href="https://erlang-slack.herokuapp.com/">here</a>)</li>
    <li><a href="mailto:zxq9@zxq9.com">Email</a></li>
    <li>Project tickets on GitLab for <a href="https://gitlab.com/zxq9/zx/issues">ZX</a>, <a href="https://gitlab.com/zxq9/zomp/issues">Zomp</a> or <a href="https://gitlab.com/zxq9/zomp-doc/issues">these docs</a></li>
  </ul>
</div>
<footer>
  <nav><a href="index.html">Top</a> | <a href="quickstart.en.html">Quickstart</a></nav>
  <p><small>&copy; 2020 Craig Everett</small></p>
  <p><small><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"><img alt="Creative Commons License" style="border-width:0" src="./by-nc-nd.png" /></a><br>
  This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.</small></p>
</footer>
</body>
</html>
